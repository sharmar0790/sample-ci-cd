package com.parking.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDateTime; 

import java.net.InetAddress;

import static com.parking.constants.ApplicationConstants.V1;

@RestController
@RequestMapping(path = V1 + "/healthz",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class HealthController {

    @GetMapping
    public String health() throws Exception {
        InetAddress inetAddress = InetAddress.getLocalHost();
        String hostAddress = inetAddress.getHostAddress();

        String json = """
                { "Status" : "OK",
                  "Version" : "GitOps",
                  "Hostname": "%s",
                  "Hostaddr": "%s",
                  "CurrentDateTime": "%s"
                 }
                """;

        return json.formatted(inetAddress.getHostName(), hostAddress,LocalDateTime.now());
    }
}